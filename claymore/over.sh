#!/bin/sh

# Core Clock : +0(0), +150(150), +200(200) 코어클럭 설정
NV_CORE=-100

# Memory Clock : +0(0), +300(600), +600(1200), +700(1400), +750(1500) 메모리클럭 설정, 윈도우기준 설정값의 두배를 넣습니다. 
NV_MEMORY=1200
 
# Fan Speed : 60%(60), 70%(70), 80%(80) 팬 스피드 설정
# Fan Control : 0은 자동, 1은 수동 
NV_FAN=70
NV_FANCTRL=1

# Power Limit(W)  
#  GTX1060(120W) : 60%(72), 65%(78), 75%(90), 78%(94)
#  GTX1070(151W) : 70%(105)
#  파워리미트 설정의 예시 이며 디폴트는 80W입니다. 와트단위로 설정합니다. %가 아닙니다.
#  GPU별로 최대 소비전력을 입력합니다. 목록에 없는경우 NV_POWERDEFAULT= 에 입력합니다.
#  P106 = POWER1060, P104 = POWER1080, P102 = POWER1080 을 따릅니다. 
NV_POWER1050=58
NV_POWER1060=76
NV_POWER1070=100
NV_POWER1080=160
NV_POWER2080=150
NV_POWER2070=110
NV_POWER2060=90
NV_POWERDEFAULT=80

nvidia-smi -pl $NV_POWERDEFAULT 

# Performance level -> GPU 
#   GTX1060 / 1070 / 1080 : 3 (default)
#   GTX1050 / P106        : 2
#   P104 / P102 / 750     : 1
#   사용중인 GPU에 맞는 설정을 자동으로 감지합니다. 디폴트는 3이며 기타 그래픽카드는 수동으로 설정해 주세요

NV_IDX=3



########################################################################
# 이 아래로는 수정하지 마세요.
########################################################################


# for nvidia-settings 
export DISPLAY=:0
export XAUTHORITY=/var/run/lightdm/root/:0
# Persistence mode
nvidia-smi -pm 1
 

 


# Total GPUs :  
GPUS=`nvidia-smi -L | wc -l`
NV_GPUS=`expr $GPUS - 1`
echo $NV_GPUS; 


#for x in $NV_GPUS; do
for x in `seq 0 $NV_GPUS`; do
NV_GPU_NAME=`nvidia-smi -i $x --query-gpu="gpu_name" --format="csv,noheader"`

echo $NV_GPU_NAME;
# GeForce GTX 1050

if [ "$(echo $NV_GPU_NAME | grep -c '1050')" -ge 1 ]; then
echo "GPU[$x] = GTX1050"
NV_IDX=2
nvidia-smi -i $x -pl $NV_POWER1050
fi
 
# GeForce GTX 1060

if [ "$(echo $NV_GPU_NAME | grep -c '1060')" -ge 1 ]; then
echo "GPU[$x] = P106"
NV_IDX=3
echo "GPU[$x] = GTX1060"
nvidia-smi -i $x -pl $NV_POWER1060
fi

# GeForce P106-100

if [ "$(echo $NV_GPU_NAME | grep -c 'P106')" -ge 1 ]; then
echo "GPU[$x] = P106"
NV_IDX=2
echo "GPU[$x] = GTX1060"
nvidia-smi -i $x -pl $NV_POWER1060
fi

 
# GeForce GTX 1070

if [ "$(echo $NV_GPU_NAME | grep -c '1070')" -ge 1 ]; then
echo "GPU[$x] = P104"
NV_IDX=3
echo "GPU[$x] = GTX1070"
nvidia-smi -i $x -pl $NV_POWER1070
fi

 
# GeForce GTX P104

if [ "$(echo $NV_GPU_NAME | grep -c 'P104')" -ge 1 ]; then
echo "GPU[$x] = P104"
NV_IDX=1
echo "GPU[$x] = GTX1080"
nvidia-smi -i $x -pl $NV_POWER1080
fi



# GeForce GTX 1080 or P102-100

if [ "$(echo $NV_GPU_NAME | grep -c '1080\|P102')" -ge 1 ]; then
echo "GPU[$x] = P102"
NV_IDX=3
echo "GPU[$x] = GTX1080"
nvidia-smi -i $x -pl $NV_POWER1080
fi

# GeForce GTX 2080 

if [ "$(echo $NV_GPU_NAME | grep -c '2080')" -ge 1 ]; then
echo "GPU[$x] = GTX2080"
NV_IDX=3
nvidia-smi -i $x -pl $NV_POWER2080
fi

# GeForce GTX 2070 

if [ "$(echo $NV_GPU_NAME | grep -c '2070')" -ge 1 ]; then
echo "GPU[$x] = GTX2070"
NV_IDX=3
nvidia-smi -i $x -pl $NV_POWER2070
fi

# GeForce GTX 2060 

if [ "$(echo $NV_GPU_NAME | grep -c '2060')" -ge 1 ]; then
echo "GPU[$x] = GTX2060"
NV_IDX=3
nvidia-smi -i $x -pl $NV_POWER2060
fi

nvidia-settings -a [gpu:$x]/GPUGraphicsClockOffset[$NV_IDX]=$NV_CORE
nvidia-settings -a [gpu:$x]/GPUMemoryTransferRateOffset[$NV_IDX]=$NV_MEMORY
nvidia-settings -a [gpu:$x]/GpuPowerMizerMode=1
nvidia-settings -a [gpu:$x]/GPUFanControlState=$NV_FANCTRL
nvidia-settings -a [fan:$x]/GPUTargetFanSpeed=$NV_FAN

done
